package utilities;

import java.security.MessageDigest;


import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA256Hasher {

  public static String stringToHash(String input){
    String output = null;

    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      md.update(input.getBytes("UTF-8"));
      byte[] digest = md.digest();
      BigInteger bigInt = new BigInteger(1, digest);
      output = bigInt.toString(16);


    } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
      System.out.println("[Sha-256Hasher] [Exception] " + ex.toString() );

      //TODO: Proper logging.
    }

    return output;
  }
}

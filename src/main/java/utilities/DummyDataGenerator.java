package utilities;


import domain.PoliceAccount;
import domain.Vehicle;
import domain.enums.Priority;
import domain.enums.Role;
import dto.PoliceAccountDto;
import service.PoliceAccountService;
import service.VehicleService;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.util.Date;

//Deze klas gebruiken om dummy data te maken i.p.v. de SQl file
//waarom? Nou als je bijv. een wachtwoord wil hashen, kun je in een SQL file neit de hasher aanroepen.
// wat in deze klas (later) wel kan
@Startup
@Singleton
public class DummyDataGenerator {

  @Inject
  VehicleService vehicleService;

  @Inject
  PoliceAccountService policeAccountService;

  @PostConstruct
  public void init() {
    this.generateDummyDate();

  }

  private void generateDummyDate(){
    System.out.println("TO DO: migrate startup-data from dummydata.sql to this method.");
    PoliceAccount p1 = new PoliceAccount("PolitieBas", "test", "De skiefelaar", "120", "5462HL", "Veghel", "Belgium", "Bas", "van Zutphen", Role.Administrator);
    PoliceAccount p2 = new PoliceAccount("PolitieDaphne", "test", "Korteldonk", "14", "8221HL", "Eindhoven", "Belgium", "Daphne", "van der Veeke", Role.Moderator);
    PoliceAccount p3 = new PoliceAccount("PolitieJim", "test", "Bloemenwijk", "23", "8923HS", "St. Michielsgestel", "Belgium", "Jim", "Vercoelen", Role.User);

    Vehicle v1 = new Vehicle("Opel", "Corsa", "21 DB LG", "1993", "RED", Boolean.TRUE, Priority.HIGH, new Date());
    Vehicle v2 = new Vehicle("BMW", "M7", "7 DJZ 29", "2010", "Blue", Boolean.TRUE, Priority.LOW, new Date());
    Vehicle v3 = new Vehicle("Mercedes", "A Klasse", "94 RP XK", "2000", "MAGNETA", Boolean.FALSE, Priority.NONE, new Date());

    policeAccountService.create(p1);
    policeAccountService.create(p2);
    policeAccountService.create(p3);

    vehicleService.create(v1);
    vehicleService.create(v2);
    vehicleService.create(v3);

    //this.smallInlineTests();

  }

  private void smallInlineTests(){
    try {
      PoliceAccount p = policeAccountService.findPoliceAccountByUserName("PolitieBas");
      PoliceAccountDto pDto = PoliceAccountDto.createPoliceAccountDto(p);
      System.out.println("[DDG] [policeaccount] "+ p.getFirstName());
      System.out.println("[DDG] [policeAccountDto] " + pDto.getFirstName());

    } catch (Exception e) {
      System.out.println("[DDG] [smallInlineTests] " + e.toString());
    }
  }
}

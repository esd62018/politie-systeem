package domain;

import domain.enums.Priority;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "VEHICLE")
@NamedQueries({
  @NamedQuery(
    name = "Vehicle.findAll",
    query = "SELECT v FROM Vehicle v"
  ),
  @NamedQuery(
    name = "Vehicle.findAllStolen",
    query = "SELECT v FROM Vehicle v WHERE v.isStolen = TRUE"
  ),
  @NamedQuery(
    name = "Vehicle.findByPriority",
    query = "SELECT v FROM Vehicle v where v.priority = :priority AND v.isStolen = TRUE"
  ),
  @NamedQuery(
    name = "Vehicle.findByLicensePlate",
    query = "SELECT v FROM Vehicle v where v.licensePlate = :licensePlate"
  )
})

public class Vehicle {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;
  @Column(name = "BRAND")
  private String brand;
  @Column(name = "MODEL")
  private String model;
  @Column(name = "LICENSE_PLATE", unique = true)
  private String licensePlate;
  @Column(name = "BUILD_YEAR")
  private String buildYear;
  @Column(name = "COLOR")
  private String color;
  @Column(name = "IS_STOLEN")
  private Boolean isStolen;
  @Column(name = "PRIORITY")
  @Enumerated(EnumType.STRING)
  private Priority priority;
  @Column(name = "STOLEN_SINCE")
  private Date stolenSince;

  public Vehicle() {

  }

  public Vehicle(String licensePlate, Boolean isStolen) {
    this.licensePlate = licensePlate;
    this.isStolen = isStolen;
  }

  public Vehicle(String brand, String model, String licensePlate, String buildYear, String color, Boolean isStolen, Priority priority, Date stolenSince) {
    this.brand = brand;
    this.model = model;
    this.licensePlate = licensePlate;
    this.buildYear = buildYear;
    this.color = color;
    this.isStolen = isStolen;
    this.priority = priority;
    this.stolenSince = stolenSince;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBrand() {
    return brand;
  }

  public void setBrand(String brand) {
    this.brand = brand;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getLicensePlate() {
    return licensePlate;
  }

  public void setLicensePlate(String licensePlate) {
    this.licensePlate = licensePlate;
  }

  public String getBuildYear() {
    return buildYear;
  }

  public void setBuildYear(String buildYear) {
    this.buildYear = buildYear;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public Boolean getIsStolen() {return isStolen; }

  public void setIsStolen(Boolean isStolen) { this.isStolen = isStolen;}

  public Priority getPriority() { return priority; }

  public void setPriority(Priority priority) { this.priority = priority; }

  public Date getStolenSince() { return stolenSince; }

  public void setStolenSince(Date stolenSince) { this.stolenSince = stolenSince; }

  @Override
  public String toString() {
    return "id: " + id
      + " brand: " + brand
      + " model: " + model
      + " license place: " + licensePlate
      + " build year: " + buildYear
      + " color: " + color
      + " isStolen: " + isStolen
      + " priority: " + priority.toString()
      + " stolen since: " + stolenSince.toString();
  }
}

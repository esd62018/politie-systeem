package domain;

import javax.persistence.*;
import domain.enums.*;

@Entity
@Table(name = "POLICE_ACCOUNT")
@NamedQueries({
  @NamedQuery(
    name = "PoliceAccount.auth",
    query = "SELECT u FROM PoliceAccount u WHERE u.userName = :username AND u.password = :password"
  ),
  @NamedQuery(
    name = "PoliceAccount.findAll",
    query = "SELECT u FROM PoliceAccount u"
  ),
  @NamedQuery(
    name = "PoliceAccount.findByID",
    query = "SELECT u FROM PoliceAccount u WHERE u.id = :id"
  ),
  @NamedQuery(
    name = "PoliceAccount.findByUserName",
    query = "SELECT u FROM PoliceAccount u WHERE u.userName = :userName"

  )
})
public class PoliceAccount {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  @Column(name = "ID")
  private Long id;
  @Column(unique = true, name = "USERNAME", nullable = false)
  private String userName;
  @Column(name = "PASSWORD", nullable = false)
  private String password;
  @Column(name = "STREET")
  private String street;
  @Column(name = "HOUSE_NUMBER")
  private String houseNumber;
  @Column(name = "ZIP_CODE")
  private String zipCode;
  @Column(name = "CITY")
  private String city;
  @Column(name = "COUNTRY")
  private String country;
  @Column(name = "FIRST_NAME")
  private String firstName;
  @Column(name = "LAST_NAME")
  private String lastName;

  @Enumerated(EnumType.STRING)
  @Column(name = "ROLE")
  private Role role;


  public PoliceAccount() {}

  //Used when registering, rest of the properties are set by default (for now) and can be modifiable in the UI
  public PoliceAccount(String userName, String password) {
    this.userName = userName;
    this.password = password;
  }

  public PoliceAccount(String userName, String password, String street, String houseNumber, String zipCode, String city, String country, String firstName, String lastName, Role role) {
    this.userName = userName;
    this.password = password;
    this.street = street;
    this.houseNumber = houseNumber;
    this.zipCode = zipCode;
    this.city = city;
    this.country = country;
    this.firstName = firstName;
    this.lastName = lastName;
    this.role = role;
  }

  public Long getId() { return id; }

  public void setId(Long id) { this.id = id; }

  public String getUserName() { return userName; }

  public void setUserName(String userName) { this.userName = userName; }

  public String getPassword() { return password; }

  public void setPassword(String password) { this.password = password; }

  public String getStreet() { return street; }

  public void setStreet(String address) { this.street = address; }

  public String getHouseNumber() { return houseNumber;}

  public void setHouseNumber(String houseNumber) {this.houseNumber = houseNumber; }

  public String getZipCode() { return zipCode; }

  public void setZipCode(String zipCode) { this.zipCode = zipCode; }

  public String getCity() { return city; }

  public void setCity(String city) { this.city = city; }

  public String getCountry() { return country; }

  public void setCountry(String country) { this.country = country; }

  public String getFirstName() { return firstName; }

  public void setFirstName(String firstName) { this.firstName = firstName; }

  public String getLastName() { return lastName; }

  public void setLastName(String lastName) { this.lastName = lastName; }

  public Role getRole() { return role; }

  public void setRole(Role role) { this.role = role; }

  @Override
  public String toString() {
    return "id: " + id
      + " username: " + userName
      + " password: " + password
      + " role " + role.toString();
  }

}

package domain;

import com.rekeningrijden.europe.interfaces.ITransLocation;

// NOTE: No entity, data should get fetched from other system
// NOTE2: Er alvast in gezet, het popo-systeem moet tevens gestolen auto's kunnen plotten.
public class TransLocation implements ITransLocation {
  private String serialNumber;
  private String dateTime;
  private String countryCode;
  private Double lat;
  private Double lon;


  public TransLocation() {

  }

  public TransLocation(Double lat, Double lon) {
    this.lat = lat;
    this.lon = lon;
  }


  public Double getLat() { return this.lat; }

  public void setLat(Double lat) {
    this.lat = lat;
  }

  public Double getLon() {
    return this.lon;
  }

  public void setLon(Double lon) {this.lon = lon; }

  @Override
  public String getDateTime() { return this.dateTime; }

  public void setDateTime(String dateTime) {this.dateTime = dateTime; }

  @Override
  public String getSerialNumber() { return this.serialNumber; }

  public void setSerialNumber(String serialNumber) {this.serialNumber = serialNumber; }

  @Override
  public String getCountryCode() { return this.countryCode; }

  public void setCountryCode(String countryCode) { this.countryCode = countryCode; }

}

package domain;

import com.rekeningrijden.europe.interfaces.IJourney;
import com.rekeningrijden.europe.interfaces.ITransLocation;

import java.util.List;

public class Journey implements IJourney {

  private List<ITransLocation> transLocations;

  @Override
  public List<ITransLocation> getTransLocations() { return this.transLocations; }

  public void setTransLocations(List<ITransLocation> transLocations) { this.transLocations = transLocations; }

}

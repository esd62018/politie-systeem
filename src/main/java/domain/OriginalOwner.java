package domain;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// deze class wordt geen entity, maar gevuld met data van een eigenaar van een gestolen auto
// Dat wil ik doen door een API-call te maken naar de RekeningRijders-app met als parameter het Kenteken.
// De API-call moet als return-waarde hebben: de attributen van de persoon wiens auto('s) is/zijn gestolen.
public class OriginalOwner {

  private String firstName;
  private String lastName;
  private String citizenServiceNumber;
  private String country;

  private List<Vehicle> stolenVehicles;

  public OriginalOwner(String firstName, String lastName, String citizenServiceNumber, String country) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.citizenServiceNumber = citizenServiceNumber;
    this.country = country;

    this.stolenVehicles = new ArrayList<Vehicle>();
  }

  public OriginalOwner(String firstName, String lastName, String citizenServiceNumber, String country, List<Vehicle> stolenVehicles) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.citizenServiceNumber = citizenServiceNumber;
    this.country = country;

    this.stolenVehicles = stolenVehicles;
  }

  public String getFirstName() { return firstName; }

  public void setFirstName(String firstName) { this.firstName = firstName; }

  public String getLastName() { return lastName; }

  public void setLastName(String lastName) {this.lastName = lastName; }

  public String getCitizenServiceNumber() { return citizenServiceNumber; }

  public void setCitizenServiceNumber(String citizenServiceNumber) {this.citizenServiceNumber = citizenServiceNumber; }

  public String getCountry() { return country; }

  public void setCountry(String country) { this.country = country; }

  public List<Vehicle> getStolenVehicles() { return stolenVehicles; }

  public void setStolenVehicles(List<Vehicle> vehicles) { this.stolenVehicles = stolenVehicles; }

  public void addToStolenVehicles(Vehicle v) {
    if (!this.stolenVehicles.contains(v)) {
      this.stolenVehicles.add(v);
    } else {
      System.out.println("Vehicle " + v.getModel() + " -- " + v.getBrand() + " already reported stolen" );
    }
  }

}

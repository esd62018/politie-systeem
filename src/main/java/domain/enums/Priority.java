package domain.enums;

public enum Priority {
  NONE,
  LOW,
  MEDIUM,
  HIGH
}

package domain.enums;

public enum Role {
  User,
  Moderator,
  Administrator,
}

package rest;

import domain.PoliceAccount;
import dto.PoliceAccountDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import service.PoliceAccountService;
import utilities.Constants;

import javax.ejb.EJBException;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/auth")
@Stateless
public class AuthRest {
  private static final String DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
  private static final DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

  @Inject
  private PoliceAccountService policeAccountService;

  @POST
  @Path("/login")
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response authenticate(PoliceAccount account) {
    ResponseBody responseBody = new ResponseBody();
    int status = 200;

    try {
      PoliceAccountDto accountDto = PoliceAccountDto.createPoliceAccountDto(policeAccountService.login(
        account.getUserName(),
        account.getPassword()
      ));
      LocalDateTime localDateTime = new Date()
        .toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDateTime()
        .plusDays(3);

      Date expiration = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

      String token = Jwts.builder()
        .setExpiration(expiration)
        .setSubject(String.valueOf(accountDto.getId()))
        .signWith(SignatureAlgorithm.HS512, Constants.JWS_SECRET)
        .compact();

      responseBody.setAuth(accountDto);
      responseBody.setExpireDate(dateFormat.format(expiration));
      responseBody.setToken(token);

    } catch (Exception e) {
      if (e instanceof EJBTransactionRolledbackException) {   // Caused by no account found
        status = 404;
      } else {
        status = 500;
      }
    }

    return Response
      .status(status)
      .entity(responseBody)
      .build();
  }

  @POST
  @Path("/sign-up")
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response signUp(PoliceAccount account) {
    PoliceAccountDto accountDto = null;
    int status = 200;

    try {
      accountDto = PoliceAccountDto.createPoliceAccountDto(policeAccountService.create(account));
    } catch (Exception e) {
      if (e instanceof EJBException) {  // Caused by constraint
        status = 404;
      } else {
        status = 500;
      }
    }

    return Response
      .status(status)
      .entity(accountDto)
      .build();
  }

  public class ResponseBody {
    private PoliceAccountDto auth;
    private String token;
    private String expireDate;

    public ResponseBody() {

    }

    public PoliceAccountDto getAuth() {
      return auth;
    }

    public void setAuth(PoliceAccountDto auth) {
      this.auth = auth;
    }

    public String getToken() {
      return token;
    }

    public void setToken(String token) {
      this.token = token;
    }

    public String getExpireDate() {
      return expireDate;
    }

    public void setExpireDate(String expireDate) {
      this.expireDate = expireDate;
    }
  }
}

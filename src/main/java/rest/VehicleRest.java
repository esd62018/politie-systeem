package rest;

import domain.Vehicle;
import domain.enums.Priority;
import dto.PoliceAccountDto;
import dto.VehicleDto;
import service.PoliceAccountService;
import service.VehicleService;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/vehicle")
@Stateless
public class VehicleRest {

  @Inject
  private VehicleService vehicleService;

  public VehicleRest() { }

  @GET
  @Path("/all")
  @Produces({APPLICATION_JSON})
  public Response getAllVehicles() {
    VehicleDto vDto = null;
    List<Vehicle> allVehicles;
    List<VehicleDto> allVehiclesDto = new ArrayList<>();
    int status = 200;

    try {
      allVehicles = vehicleService.findAllVehicles();
      for (Vehicle v : allVehicles) {
        vDto = vDto.createVehicleDto(v);
        allVehiclesDto.add(vDto);
      }
    } catch (Exception ex) {
      System.out.println("[VehicleRest] [getAllVehicles] [Error] " + ex.toString());
      status = 500;
    }
    return Response.status(status).entity(allVehiclesDto).build();
  }

  @GET
  @Path("/stolen")
  @Produces({APPLICATION_JSON})
  public Response getAllStolenCars() {
    VehicleDto vDto = null;
    List<Vehicle> allStolenVehicles;
    List<VehicleDto> allStolenVehiclesDto = new ArrayList<>();
    int status = 200;

    try {
      allStolenVehicles = vehicleService.findAllStolenVehicles();
      for (Vehicle v : allStolenVehicles) {
        vDto = vDto.createVehicleDto(v);
        allStolenVehiclesDto.add(vDto);
      }
    } catch (Exception ex) {
      System.out.println("[VehicleRest] [getAllVehicles] [Error] " + ex.toString());
      status = 500;
    }
    return Response.status(status).entity(allStolenVehiclesDto).build();
  }

  @GET
  @Path("/stolen/{priority}")
  @Produces({APPLICATION_JSON})
  public Response getStolenCarsByPriority(@PathParam("priority") String priority) {
    VehicleDto vDto = null;
    List<Vehicle> StolenVehicles;
    List<VehicleDto> StolenVehiclesDto = new ArrayList<>();
    int status = 200;

    try {
      StolenVehicles = vehicleService.findVehiclesByPriority(Priority.valueOf(priority.toUpperCase()));
      for (Vehicle v : StolenVehicles) {
        vDto = vDto.createVehicleDto(v);
        StolenVehiclesDto.add(vDto);
      }
    } catch (Exception ex) {
      System.out.println("[VehicleRest] [getAllVehicles] [Error] " + ex.toString());
      status = 500;
    }
    return Response.status(status).entity(StolenVehiclesDto).build();
  }

  @GET
  @Path("/byLicensePlate/{licensePlate}")
  @Produces({APPLICATION_JSON})
  public Response getCarByLicensePlate(@PathParam("licensePlate") String licensePlate) {
    VehicleDto vDto = null;
    int status  = 200;

    try {
      vDto = vDto.createVehicleDto(vehicleService.findVehicleByLicensePlate(licensePlate));
    } catch (Exception ex) {
      System.out.println("[VehicleRest] [getCarByLicensePlate] [Error] " + ex.toString());
      status = 500;
    }
    return Response.status(status).entity(vDto).build();
  }

  @POST
  @Path("/report")
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response reportStolenVehicle(Vehicle vehicle) {
    VehicleDto vDto = null;
    int status = 200;

    try {
      vDto = vDto.createVehicleDto(vehicleService.create(vehicle));
    } catch(Exception ex) {
      System.out.println("[VehicleRest] [reportStolenVehicle] [Error] " + ex.toString());
      if (ex instanceof EJBException) {
        status = 404; // not found
      } else {
        status = 500;
      }
    }
    return Response.status(status).entity(vDto).build();
  }


}







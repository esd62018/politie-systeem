package rest;

import domain.PoliceAccount;
import dto.PoliceAccountDto;
import service.PoliceAccountService;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/police")
@Stateless
public class PoliceAccountRest {

  @Inject
  private PoliceAccountService policeAccountService;

  public PoliceAccountRest() {}

  @GET
  @Path("/byUserName/{userName}")
  @Produces({APPLICATION_JSON})
  public Response getPoliceAccountByUsername(@PathParam("userName") String userName) {
    PoliceAccountDto pDto = null;
    int status = 200;

    try {
      pDto = pDto.createPoliceAccountDto(policeAccountService.findPoliceAccountByUserName(userName));
    } catch (Exception ex) {
      System.out.println("[PoliceAccountRest] [byUsername] [Error] " + ex.toString());
      status = 500;
    }
    return Response.status(status).entity(pDto).build();
  }

  @GET
  @Path("/byID/{id}")
  @Produces({APPLICATION_JSON})
  public Response getPoliceAccountByID(@PathParam("id") long id) {
    PoliceAccountDto pDto = null;
    int status = 200;

    try {
      pDto = pDto.createPoliceAccountDto(policeAccountService.findPoliceAccountByID(id));
    } catch (Exception ex) {
      System.out.println("[PoliceAccountRest] [byID] [Error] " + ex.toString());
      status = 500;
    }
    return Response.status(status).entity(pDto).build();
  }

  @GET
  @Path("/all")
  @Produces({ APPLICATION_JSON })
  public Response getAllPoliceAccounts() {
    PoliceAccountDto pDto = null;
    List<PoliceAccount> allPoliceAccounts;
    List<PoliceAccountDto> allPoliceAccountDtos = new ArrayList<>();
    int status = 200;

    try {
      allPoliceAccounts = policeAccountService.findAll();
      for (PoliceAccount p : allPoliceAccounts) {
        pDto = pDto.createPoliceAccountDto(p);
        allPoliceAccountDtos.add(pDto);
      }
    }catch (Exception ex) {
      System.out.println("[PoliceAccountRest] [getAllPoliceAccounts] [Error] " + ex.toString());
      status = 500;
    }
    return Response.status(status).entity(allPoliceAccountDtos).build();
  }

  @DELETE
  @Path("/delete/{id}")
  @Consumes({APPLICATION_JSON})
  @Produces({APPLICATION_JSON})
  public Response delete(@PathParam("id") Long id) {
    int status = 200;

    try {
      policeAccountService.delete(id);
    } catch (Exception ex) {
      System.out.println("[PoliceAccountRest] [delete] [Error] " + ex.toString());
      status = 500;
    }
    return Response.status(status).entity("Account with ID[1] Deleted").build();
  }

  @PUT
  @Consumes({APPLICATION_JSON})
  @Produces({APPLICATION_JSON})
  public Response update(PoliceAccount policeAccount){
    //TODO
    throw new NotImplementedException();
  }

}

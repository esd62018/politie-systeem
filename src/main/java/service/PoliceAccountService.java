package service;

import dao.PoliceAccountDao;
import domain.PoliceAccount;
import domain.enums.Role;
import utilities.SHA256Hasher;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class PoliceAccountService {

  @Inject
  private PoliceAccountDao policeAccountDao;

  public PoliceAccountService() {

  }

  public PoliceAccount login(String userName, String password) {
    return policeAccountDao.login(userName, password);
  }

  public PoliceAccount findPoliceAccountByID(long id) {
    return policeAccountDao.findPoliceAccountByID(id);
  }

  public PoliceAccount findPoliceAccountByUserName(String userName) {
    return policeAccountDao.findPoliceAccountByUserName(userName);
  }

  public List<PoliceAccount> findAll() {
    return policeAccountDao.findAll();
  }

  public PoliceAccount create(PoliceAccount account) {
    //VERPLICHT: username & password

    if (account.getCity() == null) {account.setCity("testCity"); }
    if (account.getCountry() == null) {account.setCountry("Belgium"); }
    if (account.getFirstName() == null) {account.setFirstName("testFirstname"); }
    if (account.getLastName() == null) {account.setLastName("testLastname"); }
    if (account.getHouseNumber() == null) {account.setHouseNumber("120"); }
    if (account.getRole() == null) {account.setRole(Role.User); }
    if (account.getStreet() == null) {account.setStreet("testStreet"); }
    if (account.getZipCode() == null) {account.setZipCode("5221T"); }

    return policeAccountDao.create(account);
  }

  public PoliceAccount update(PoliceAccount account) {
    return policeAccountDao.update(account);
  }

  public void delete(Long id) {
    policeAccountDao.delete(id);
  }
}

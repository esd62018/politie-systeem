package service;

import dao.VehicleDao;
import domain.Vehicle;
import domain.enums.Priority;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class VehicleService {

  @Inject
  private VehicleDao vehicleDao;

  public List<Vehicle> findAllVehicles() {
    return vehicleDao.findAllVehicles();
  }

  public List<Vehicle> findAllStolenVehicles() {
    return vehicleDao.findAllStolenVehicles();
  }

  public List<Vehicle> findVehiclesByPriority(Priority priority) {
    return vehicleDao.findVehiclesByPriority(priority);
  }

  public Vehicle findVehicleByLicensePlate(String licensePlate) {
    return vehicleDao.findVehicleByLicensePlate(licensePlate); }

  public Vehicle create(Vehicle v) {
    // VERPLICHT: licensePlate && isStolen

    if (v.getBrand() == null ) {v.setBrand("TestBrand"); }
    if (v.getBuildYear() == null) {v.setBuildYear("1922"); }
    if (v.getColor() == null) {v.setColor("Orange"); }
    if (v.getModel() == null) {v.setModel("testModel"); }
    if (v.getPriority() == null){v.setPriority(Priority.HIGH); }

    return vehicleDao.create(v);
  }

  public Vehicle update(Vehicle v) {
    return vehicleDao.update(v);
  }

  public void delete(long id) {
    vehicleDao.delete(id);
  }



}

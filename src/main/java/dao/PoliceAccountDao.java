package dao;

import domain.PoliceAccount;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import utilities.SHA256Hasher;
import domain.PoliceAccount;

import java.util.List;

@Stateless
public class PoliceAccountDao {

  @PersistenceContext
  private EntityManager em;

  public PoliceAccountDao() {}

  public PoliceAccount login(String userName, String password) {
      System.out.println("[PoliceAccountDao] [login] loginname: " + userName);
      System.out.println("[PoliceAccountDao] [login] password: " + password);
      PoliceAccount p = null;

      try {
        // two steps to login:
        // first get account record by username
        // then check password manually
        // (this because the client can call this functionality with plain password - login, or hashed password - reload)

        Query q = em.createNamedQuery("PoliceAccount.findByUserName", PoliceAccount.class);
        q.setParameter("userName", userName);
        p = (PoliceAccount) q.getSingleResult();

        if (p != null) {
          if (p.getPassword().equals(password) ||
            p.getPassword().equals(SHA256Hasher.stringToHash(password))) {
            return p;
          }
        }

        // Password doesn't match
        return null;
      } catch (NoResultException ex) {
        System.out.println("[PoliceAccountDao] [login] [No results for the given Username - password combination]" + ex.toString());
      }
      return p;
  }

  public List<PoliceAccount> findAll() {
    return em
      .createNamedQuery("PoliceAccount.findAll", PoliceAccount.class)
      .getResultList();
  }

  public PoliceAccount findPoliceAccountByID(long id) {
      return em
        .createNamedQuery("PoliceAccount.findByID", PoliceAccount.class)
        .setParameter("id", id)
        .getSingleResult();
  }

  public PoliceAccount findPoliceAccountByUserName(String userName) {
      return em
        .createNamedQuery("PoliceAccount.findByUserName", PoliceAccount.class)
        .setParameter("userName", userName)
        .getSingleResult();
  }


  public PoliceAccount create(PoliceAccount account) {
    String rawPw = account.getPassword();
    String hashedPW = SHA256Hasher.stringToHash(rawPw);
    account.setPassword(hashedPW);

    em.persist(account);

    //in early stages the account was returned directly: But that did obviously not include it's ID (which is automaticly set by JPA), therefore:
    PoliceAccount returnAccount = this.findPoliceAccountByUserName(account.getUserName());
    return returnAccount;
  }

  public PoliceAccount update(PoliceAccount account) {
      em.merge(account);
      return account;
  }

  public void delete(Long id) {
    em.remove(em.find(PoliceAccount.class, id));
  }
}

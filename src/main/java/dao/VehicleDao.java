package dao;

import domain.Vehicle;
import domain.enums.Priority;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class VehicleDao {

  @PersistenceContext
  private EntityManager em;

  public List<Vehicle> findAllVehicles() {
    return em
      .createNamedQuery("Vehicle.findAll", Vehicle.class)
      .getResultList();
  }

  public List<Vehicle> findAllStolenVehicles() {
    return em
      .createNamedQuery("Vehicle.findAllStolen", Vehicle.class)
      .getResultList();
  }

  public List<Vehicle> findVehiclesByPriority(Priority priority) {
    return em
      .createNamedQuery("Vehicle.findByPriority", Vehicle.class)
      .setParameter("priority", priority)
      .getResultList();
  }

  public Vehicle findVehicleByLicensePlate(String licensePlate) {
    return em
      .createNamedQuery("Vehicle.findByLicensePlate", Vehicle.class)
      .setParameter("licensePlate", licensePlate)
      .getSingleResult();
  }

  public Vehicle create(Vehicle v) {
    em.persist(v);

    //in early stages the Vehicle was returned directly: But that did obviously not include it's ID (which is automaticly set by JPA), therefore:
    Vehicle returnVehicle = this.findVehicleByLicensePlate(v.getLicensePlate());
    return returnVehicle;
  }

  public Vehicle update(Vehicle v) {
    em.merge(v);
    return v;
  }

  public void delete(Long id) {
    em.remove(em.find(Vehicle.class, id));
  }
}

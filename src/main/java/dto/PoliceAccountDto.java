package dto;

import domain.PoliceAccount;

public class PoliceAccountDto {

  private String id;
  private String userName;
  private String password;
  private String street;
  private String houseNumber;
  private String zipCode;
  private String city;
  private String country;
  private String firstName;
  private String lastName;
  private String role;

  //laten staan pl0x
  public PoliceAccountDto() {}

  public PoliceAccountDto(String id, String userName, String password, String street, String houseNumber, String zipCode, String city, String country, String firstName, String lastName, String role ) {
    this.id = id;
    this.userName = userName;
    this.password = password;
    this.street = street;
    this.houseNumber = houseNumber;
    this.zipCode = zipCode;
    this.city = city;
    this.country = country;
    this.firstName = firstName;
    this.lastName = lastName;
    this.role = role;
  }

  public static PoliceAccountDto createPoliceAccountDto(PoliceAccount policeAccount){
    return new PoliceAccountDto(
      policeAccount.getId().toString(),
      policeAccount.getUserName(),
      policeAccount.getPassword(),
      policeAccount.getStreet(),
      policeAccount.getHouseNumber(),
      policeAccount.getZipCode(),
      policeAccount.getCity(),
      policeAccount.getCountry(),
      policeAccount.getFirstName(),
      policeAccount.getLastName(),
      policeAccount.getRole().toString()
    );
  }

  public String getId() { return id; }

  public void setId(String id) { this.id = id; }

  public String getUserName() { return userName; }

  public void setUserName(String userName) { this.userName = userName; }

  public String getPassword() { return password; }

  public void setPassword(String password) { this.password = password; }

  public String getStreet() { return street; }

  public void setStreet(String address) { this.street = address; }

  public String getHouseNumber() { return houseNumber;}

  public void setHouseNumber(String houseNumber) {this.houseNumber = houseNumber; }

  public String getZipCode() { return zipCode; }

  public void setZipCode(String zipCode) { this.zipCode = zipCode; }

  public String getCity() { return city; }

  public void setCity(String city) { this.city = city; }

  public String getCountry() { return country; }

  public void setCountry(String country) { this.country = country; }

  public String getFirstName() { return firstName; }

  public void setFirstName(String firstName) { this.firstName = firstName; }

  public String getLastName() { return lastName; }

  public void setLastName(String lastName) { this.lastName = lastName; }

  public String getRole() { return role; }

  public void setRole(String role) { this.role = role; }
}

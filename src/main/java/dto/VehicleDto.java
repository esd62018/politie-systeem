package dto;

import domain.PoliceAccount;
import domain.Vehicle;

public class VehicleDto {


  private String id;
  private String brand;
  private String model;
  private String licensePlate;
  private String buildYear;
  private String color;
  private String isStolen;
  private String priority;

  //laten staan pl0x
  public VehicleDto() {}

  public VehicleDto(String id, String brand, String model, String buildYear, String licensePlate, String color, String isStolen, String priority){
    this.id = id;
    this.brand = brand;
    this.model = model;
    this.buildYear = buildYear;
    this.licensePlate = licensePlate;
    this.color = color;
    this.isStolen = isStolen;
    this.priority = priority;
  }

  public static VehicleDto createVehicleDto(Vehicle vehicle){
    return new VehicleDto(
      vehicle.getId().toString(),
      vehicle.getBrand(),
      vehicle.getModel(),
      vehicle.getBuildYear(),
      vehicle.getLicensePlate(),
      vehicle.getColor(),
      Boolean.toString(vehicle.getIsStolen()),
      vehicle.getPriority().toString()
      );
  }


  public String getId() { return id; }

  public void setId(String id) { this.id = id; }

  public String getBrand() { return brand; }

  public void setBrand(String brand) { this.brand = brand; }

  public String getModel() { return model; }

  public void setModel(String model) { this.model = model; }

  public String getLicensePlate() { return licensePlate; }

  public void setLicensePlate(String licensePlate) { this.licensePlate = licensePlate; }

  public String getBuildYear() { return buildYear; }

  public void setBuildYear(String buildYear) { this.buildYear = buildYear; }

  public String getColor() { return color; }

  public void setColor(String color) { this.color = color; }

  public String getIsStolen() { return isStolen; }

  public void setIsStolen(String isStolen) { this.isStolen = isStolen; }

  public String getPriority() { return priority; }

  public void setPriority(String priority) { this.priority = priority; }
}
